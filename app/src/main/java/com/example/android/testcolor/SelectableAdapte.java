package com.example.android.testcolor;


import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

class SelectableAdapter extends RecyclerView.Adapter {
    private final List<SelectableItem> mValues;

    public SelectableAdapter(List<Item> items, boolean isMultiSelectionEnabled) {
        mValues = new ArrayList<>();
        for (Item item : items) {
            mValues.add(new SelectableItem(item, false));
        }
    }

    @Override
    public SelectableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checked_text_item, parent, false);
        return new SelectableViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        final SelectableViewHolder holder = (SelectableViewHolder) viewHolder;

        SelectableItem selectableItem = mValues.get(position);
        String name = selectableItem.getColor();
        String visibleColor = selectableItem.getVisibleColor();
        holder.textView.setText(name);
        TypedValue value = new TypedValue();
        holder.textView.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorSingle, value, true);
        holder.cardView.getContext().getTheme().resolveAttribute(android.R.attr.listChoiceIndicatorSingle, value, true);
        holder.mItem = selectableItem;

        if (holder.mItem.isSelected()) {
            holder.cardView.setCardBackgroundColor(Color.parseColor(visibleColor));
            holder.cardView.setMinimumHeight(300);
            holder.textView.setTextColor(Color.BLACK);
        } else {
            holder.cardView.setCardBackgroundColor(Color.GRAY);
            holder.cardView.setMinimumHeight(40);
            holder.textView.setTextColor(Color.parseColor(visibleColor));
        }

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.mItem.setSelected(!holder.mItem.isSelected());
                holder.textView.setChecked(!holder.mItem.isSelected());
                Log.d(TAG, "onBindViewHolder: выбран элимент ");
                //for redrawing
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public List<Item> getSelectedItems() {

        List<Item> selectedItems = new ArrayList<>();
        for (SelectableItem item : mValues) {
            if (item.isSelected()) {
                selectedItems.add(item);
            }
        }
        return selectedItems;
    }

    @Override
    public int getItemViewType(int position) {
        return SelectableViewHolder.SINGLE_SELECTION;
    }
}
