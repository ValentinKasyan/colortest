package com.example.android.testcolor;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckedTextView;

public class SelectableViewHolder extends RecyclerView.ViewHolder {

    public static final int SINGLE_SELECTION = 1;
    CheckedTextView textView;
    CardView cardView;
    SelectableItem mItem;


    public SelectableViewHolder(View view) {
        super(view);
        cardView = (CardView) view.findViewById(R.id.cardView_text_item);
        textView = (CheckedTextView) view.findViewById(R.id.checked_text_item);
    }
}