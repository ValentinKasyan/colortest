package com.example.android.testcolor;


public class SelectableItem extends Item {
    private boolean isSelected = false;


    public SelectableItem(Item item, boolean isSelected) {
        super(item.getColor(), item.getVisibleColor());
        this.isSelected = isSelected;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}