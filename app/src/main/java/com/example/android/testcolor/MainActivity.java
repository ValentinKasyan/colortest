package com.example.android.testcolor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.CheckedTextView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SelectableAdapter adapter;
    CheckedTextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView = (RecyclerView) this.findViewById(R.id.selection_list);
        textView = (CheckedTextView) findViewById(R.id.checked_text_item);
        recyclerView.setLayoutManager(layoutManager);
        List<Item> selectableItems = generateItems();
        adapter = new SelectableAdapter(selectableItems, false);
        recyclerView.setAdapter(adapter);

    }

    public List<Item> generateItems() {
        List<Item> selectableItems = new ArrayList<>();
        selectableItems.add(new Item("red", "#d92a27"));
        selectableItems.add(new Item("blue", "#338894"));
        selectableItems.add(new Item("green", "#3b4b14"));
        selectableItems.add(new Item("Yellow", "#f5d86b"));
        selectableItems.add(new Item("Brown", "#874124"));
        selectableItems.add(new Item("Violet", "#854ac1"));
        selectableItems.add(new Item("Turquoise", "#338894"));
        return selectableItems;
    }
}

