package com.example.android.testcolor;

public class Item {
    private String color;
    private String visibleColor;

    public Item(String color, String visibleColor) {
        this.color = color;
        this.visibleColor = visibleColor;
    }


    public String getVisibleColor() {
        return visibleColor;
    }

    public void setVisibleColor(String visibleColor) {
        this.visibleColor = visibleColor;
    }

    public String getColor() {
        return color;
    }

    public void setName(String name) {
        this.color = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        Item itemCompare = (Item) obj;
        if (itemCompare.getColor().equals(this.getColor()))
            return true;

        return false;
    }
}
